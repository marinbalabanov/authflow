# Auth-Flow

## German Version
This file contains the original German version of this concept. For the **English version**, please see the file `README_EN.md`.

## Über dieses Konzept
Hierbei handelt es sich um eine detaillierte Beschreibung der Authentifizierung für den Registrierungs- und Login‐Bereich der hypothetischen Web‐Applikation xyz.com zu Übungszwecken. Hier seht die weibliche Form "Benutzerin" für beide Geschlechter verwendet.

Für diesen Prozess werden drei Komponenten benötigt:

- Frontend (client-seitig)
- Captcha-Service
- Backend (server-seitig)

## Inhaltsverzeichnis

[User Journey](#user-journey)

- [1. Registrierung](#1-registrierung)
- [2. Login](#2-login)
- [3. Passwort vergessen](#3-passwort-vergessen)

[Sicherheitsbetrachtungen](#sicherheitsbetrachtungen)

[Flowcharts](#flowcharts)

- [Flowchart: Registrierung](#flowchart-registrierung)
- [Flowchart: Login](#flowchart-login)
- [Flowchart: Passwort vergessen](#flowchart-passwort-vergessen)

[Out of Scope: Nicht in dieser Beschreibung berücksichtigt](#out-of-scope-nicht-in-dieser-beschreibung-berücksichtigt)

## User Journey

### 1. Registrierung

Registrierung einer neuen Benutzerin mit Email-Adresse und Passwort für die web app xyz.com. Das Passwort muss unseren Mindestanforderungen erfüllen (Min. 8 Zeichen Länge, gemischt Buchstaben/Zahlen/Sonderzeichen). Das Passwort wird im Backend nicht im Klartext gespeichert, sondern nur in gehashter Form. Nach erfolgreicher Registrierung wird eine Bestätigungs-Email an die angegebene Email-Adresse gesendet. Erst nach Klicken des Bestätigungslinks ist die Registrierung abgeschlossen.

**A. Eingabe der Credentials:** Die Benutzerin gibt folgende Informationen ein:

- Email-Adresse
- Passwort (und Passwort-Bestätigung)
- Captcha (generiertes Bild mit verformtem Textinhalt, den die Benutzerin eingeben muss)
- Optional: Vorname und Nachname.
- AGB akzeptieren (der Submit-Button wird nur aktiviert, wenn die AGB akzeptiert wurden)

**Anmerkung:** Der Captcha-Service könnte hier eine eigene Implementierung sein (z.B. mit einem eigenen Node.js-Server), oder ein externer Service. Aus Sicherheitsgünden, wäre hier von Google reCaptcha abzuraten. Es gibt aber auch andere Captcha-Services, die nicht von Google sind.

**B. Client-seitige Validierung:** Die im Formular eingegebenen Informationen werden vor dem verschicken auf Client-Seite validiert:

- Überprüfung auf gültiges Email-Format.
- Überprüfung des Passworts auf Mindestanforderungen (Min. 8 Zeichen Länge, gemischt Buchstaben/Zahlen/Sonderzeichen).
- Überprüfung, ob das Passwort und die Passwort-Bestätigung übereinstimmen.
- Überprüfung, ob der Captcha-Text korrekt eingegeben wurde.

**C. Server-seitige Validierung:** Der Server validiert die eingegebenen Informationen:

- Überprüfung, ob diese Email-Adresse bereits verwendet wird. Wenn die Email-Adresse bereits verwendet wird, dann wird eine entsprechende Meldung angzeigt und auf die Login-Seite weitergeleitet.

**D. Passwort-Hashing:** Das eingegebene Passwort wird nie im Klartext gespeichert. Stattdessen wird es mit einem kryptografischen Hashing-Algorithmus (z.B. bcrypt) gehasht.

**E. Datenbank-Speicherung:** Die gehashte Version des Passworts und die Email-Adresse des Benutzers werden in der Datenbank gespeichert.

**F. Bestätigungs-Email:** Eine Email mit einem Bestätigungslink wird an die angegebene Adresse gesendet. Wenn die Benutzerin auf den Link in der Email klickt, dann wird eine Bestätigungsseite geöffnet, somit ist die Registrierung abgeschlossen. Die Benutzerin kann sich nun mit ihrer Email-Adresse und ihrem Passwort anmelden.

Allerdings verliert der Link nach 24 Stunden seine Gültigkeit. Wenn die Benutzerin den Link nach Ablauf der Gültigkeit klickt, dann wird eine Seite mit einer Fehlermeldung angezeigt und die Möglichkeit gegeben, das Passwort zurückzusetzen.

### Wireframes: Registrierung

![Registrierung](diagrams/1_registration.png)

### 2. Login

Die Benutzerin kann sich mit ihrer Email-Adresse und ihrem Passwort anmelden Nach erfolgreicher Anmeldung wird eine Sitzung für die Benutzerin erstellt. An dieser Stelle wird auch überprüft, ob die Benutzerin bereits eingeloggt ist. Dafür wird der JWT-Token aus dem LocalStorage des Clients ausgelesen und an den Server geschickt. Der Server überprüft den Token und entscheidet, ob die Anfrage erlaubt ist oder nicht. Wenn nicht, dann bliebt der Client auf der Login-Seite.

**A. Eingabe der Credentials:** Die Benutzerin gibt ihre Email-Adresse und ihr Passwort ein. An dieser Stelle wird überprüft, ob der Client einen gülitgen und aktuellen JWT-Token im LocalStorage gespeichert hat. Wenn ja, dann wird dieser an den Server geschickt und die Benutzerin wird automatisch eingeloggt.

**B. Datenbankabfrage:** Der Server sucht in der Datenbank nach der gehashten Version des eingegebenen Passworts und der Email-Adresse.

**C. Passwort-Überprüfung:** Der Server vergleicht das gehashte Passwort in der Datenbank mit dem gehashten Wert des eingegebenen Passworts. Bei Übereinstimmung ist die Authentifizierung erfolgreich.

**D. Sitzungserstellung:** Bei erfolgreicher Authentifizierung erstellt der Server eine Sitzung für die Benutzerin. Dies wird durch ein JWT (JSON Web Token) realisiert.

**E. JWT-Token:** Der Server sendet den JWT-Token an den Client. Dieser wird im LocalStorage des Clients gespeichert und bei jeder Anfrage an den Server mitgeschickt.

### Wireframes: Login

![Login](diagrams/2_login.png)

### 3. Passwort vergessen

Die Benutzerin kann ein neues Passwort anfordern, wenn sie ihr altes vergessen hat. Dazu muss sie ihre Email-Adresse angeben. Nach Eingabe der Email-Adresse wird eine Bestätigungs-Email an diese Email-Adresse gesendet. Erst nach Klicken des Bestätigungslinks kann ein neues Passwort eingegeben werden.

**A. UI-Eingabe:** Die Benutzerin gibt ihre Email-Adresse ein.

**B. Bestätigungs-Email:** Eine Email mit einem einmaligen Link wird an die angegebene Adresse gesendet. Dieser Link ist für 24 Stunden gültig.

**C. Passwort-vergessen-Seite:** Durch Klicken auf den Link in der Email gelangt der Benutzer auf eine sichere Webseite, auf der er ein neues Passwort eingeben kann. Wenn die Benutzerin den Link nach Ablauf der Gültigkeit klickt, dann wird eine Seite mit einer Fehlermeldung angezeigt und die Möglichkeit gegeben, erneut das Passwort zurückzusetzen.

**D. Passwort-Änderung:** Nach Eingabe und Bestätigung des neuen Passworts wird dieses (nach erneutem Hashing) in der Datenbank aktualisiert.

### Wireframes: Passwort vergessen

![Passwort zurücksetzen](diagrams/3_pw_reset.png)

## Sicherheitsbetrachtungen

In diesem Registrierung/Login-Prozess wird ein JSON Web Token (JWT) für die Authentifizierung, Autorisierung und zum sicheren Übertragen von Informationen. Der Token wird im LocalStorage des Clients gespeichert und läuft nach 24 Stunden ab. Der Token wird bei jeder Anfrage an den Server mitgeschickt. Der Server überprüft den Token und entscheidet, ob die Anfrage erlaubt ist oder nicht. Wenn nicht, dann wird der Client zur Login-Seite weitergeleitet.

Die Schritte des JWT-Tokenprozesses sind in den Flowcharts berücksichtigt.

## Flowcharts

### Flowchart: Registrierung

![Registrierungs-Workflow](diagrams/a_registration_flowchart.png)

### Flowchart: Login

![Login-Workflow](diagrams/b_login_flowchart.png)

### Flowchart: Passwort vergessen

![PW-Reset-Workflow](diagrams/c_pw_reset_flowchart.png)

## Out of Scope: Nicht in dieser Beschreibung berücksichtigt

In dieser Beschreibung wird nur die Registrierung eines Standard-Users beschrieben. Es wird nicht auf die Registrierung eines Admin-Users eingegangen. Dieser könnte z.B. zusätzliche Informationen wie die Rolle (Admin) und die Berechtigungen enthalten.

Außerdem wird hier nicht beschrieben, ob bei mehrfach falsch eingegebenem Passwort eine Email an die Benutzerin gesendet werden könnte, um sie zu informieren, oder vielleicht sogar das Login für eine Zeit zu sperren.

Zudem wird hier nicht ausdrücklich auf die Passwort-Änderung eingegangen. Diese würde sich aber ähnlich wie der Passwort-Zurücksetzen-Prozess abspielen.
